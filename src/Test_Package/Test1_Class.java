package Test_Package;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger_Class;
import Common_Methods.Utility_Class;
import Repository_Package.RequestBody_Class;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test1_Class extends RequestBody_Class {
	public static void executor() throws IOException {
		File dirname = Utility_Class.logdirectory("Log_Directory");
		String req_body = RequestBody_Class.Test_Case1("Post_TC1");
		String endpoint = RequestBody_Class.Hostname() + RequestBody_Class.resource();
		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = API_Trigger_Class.post_trigger(RequestBody_Class.headername(),
					RequestBody_Class.headervalue(), req_body, endpoint);
			statuscode = response.statusCode();
			if (statuscode == 201) {
				Utility_Class.logfilecreator(Utility_Class.logname("Test1_Class"), dirname, endpoint, req_body,
						response.getHeader("Date"), response.getBody().asString());
				validator(response, req_body);
				break;
			} else {
				System.out.println("Statuscode is not matched" + i);

			}
		}
		if (statuscode != 201) {
			System.out.println("Expected status code is not found after all iterations");
			Assert.assertEquals(statuscode, 200);
		}
	}

	public static void validator(Response response, String req_body) {
		ResponseBody res_body = response.getBody();
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_id = res_body.jsonPath().getString("id");
		String res_createdat = res_body.jsonPath().getString("createdAt");
		res_createdat = res_createdat.substring(0, 11);
		JsonPath jsp_req = new JsonPath(req_body);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currdate = LocalDateTime.now();
		String exp_date = currdate.toString().substring(0, 11);
		Assert.assertEquals(response.statusCode(), 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, exp_date);

	}
}
