package Common_Methods;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger_Class {
	public static Response post_trigger(String headername, String headervalue, String reqbody, String endpoint)
	{
		RequestSpecification req_spec=RestAssured.given();
		req_spec.header(headername,headervalue);
		req_spec.body(reqbody);
		Response response=req_spec.post(endpoint);
		return response;
		
	}

}
