package Common_Methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility_Class {
	public static ArrayList<String> exceldata(String sheetname, String testcase) throws IOException {
		ArrayList<String> arraydata = new ArrayList<String>();
		String prj_dir = System.getProperty("user.dir");
		System.out.println("Project Directory is : " + prj_dir);
		FileInputStream fis = new FileInputStream(prj_dir + "\\Data_Files\\Data_Driven_File.xlsx");

		XSSFWorkbook xwb = new XSSFWorkbook(fis);
		int count = xwb.getNumberOfSheets();
		System.out.println("Number of Sheets in Excel: " + count);
		for (int i = 0; i < count; i++)
		{
			if (xwb.getSheetName(i).equals(sheetname))
			{
				System.out.println("Sheet at Index   : " + i + ":" + xwb.getSheetName(i));
				XSSFSheet datasheet = xwb.getSheetAt(i); // access row from sheet
				Iterator<Row> rows = datasheet.iterator();
				String testcasefound = "False";
				while (rows.hasNext()) {
					Row datarow = rows.next();
					String testcasename = datarow.getCell(0).getStringCellValue();
					if (testcasename.equals(testcase)) {
						testcasefound = "True";
						Iterator<Cell> cellvalues = datarow.cellIterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							arraydata.add(testdata);
						}
						break;

					}

				}
				if (testcasefound.equals("False")) {
					System.out.println("Test Case not Found" + xwb.getSheetName(i));
				}
				break;
			} else {
				System.out.println("Sheet Not Found in Excel Sheet" + i);

			}
		}
		xwb.close();
		return arraydata;
	}

	public static void logfilecreator(String filename, File filelocation, String endpoint, String requestbody,
			String res_header, String responsebody) throws IOException {

		File newTextFile = new File(filelocation + "\\" + filename + ".txt");
		System.out.println("File create with name: " + newTextFile.getName());
		
		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("EndPoint is :" + endpoint);
		writedata.write("Request body is" + requestbody);
		writedata.write("Response body is" + responsebody);
		writedata.write("Response Header is :" + res_header);
		writedata.close();
	}

	public static File logdirectory(String dirname) {
		String proj_dir = System.getProperty("user.dir");
		File directory = new File(proj_dir + "\\" + dirname);
		if (directory.exists()) {
			System.out.println("Directory Already Exists");
		} else {
			System.out.println("Create New Directory for log");
			directory.mkdir();
		}
		return directory;
	}

	public static String logname(String name) {
		LocalTime currenttime = LocalTime.now();
		String currentstringtime = currenttime.toString().replaceAll(":", "");
		String testlogname = "Test_Case" + currentstringtime;
		return testlogname;

	}
}