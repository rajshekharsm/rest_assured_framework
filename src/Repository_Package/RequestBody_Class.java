package Repository_Package;

import java.io.IOException;
import java.util.ArrayList;

import Common_Methods.Utility_Class;

public class RequestBody_Class extends Environment_Class {
	public static String Test_Case1(String testcase) throws IOException {
		ArrayList<String> data = Utility_Class.exceldata("Post", testcase);
		String key_name = data.get(1);
		String value_name = data.get(2);
		String key_job = data.get(3);
		String value_job = data.get(4);
		String req_body = "{\r\n" + "\"" + key_name + "\": \"" + value_name + "\",\r\n" + "\"" + key_job + "\":\""
				+ value_job + " \"\r\n" + "}";
		return req_body;
	}
}
