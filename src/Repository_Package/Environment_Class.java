package Repository_Package;

public class Environment_Class {
	public static String Hostname() {
		String Hostname = "https://reqres.in/";
		return Hostname;
	}

	public static String resource() {
		String resource = "api/users";
		return resource;
	}

	public static String headervalue() {
		String headervalue = "Application/Json";
		return headervalue;
	}

	public static String headername() {
		String headername = "Content-Type";
		return headername;
	}
}
